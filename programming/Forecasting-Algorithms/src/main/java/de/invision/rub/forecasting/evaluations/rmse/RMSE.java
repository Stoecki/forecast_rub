package de.invision.rub.forecasting.evaluations.rmse;

import java.util.HashMap;
import java.util.Map;

import de.invision.rub.forecasting.data.DataPoint;
import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.evaluations.Evaluation;

/** 
 * The class {@code RMSE} computes the "root mean square error" (RMSE). 
 */
public class RMSE implements Evaluation
{
	@Override
	public Map<DataPointType, Double> evaluate(TimeSeriesDataset actualValues, TimeSeriesDataset forecastValues)
	{
		Map<DataPointType, Double> rmse = new HashMap<DataPointType, Double>();
		
		DataPointType[] types = actualValues.getTypes();
		
		int nValues = actualValues.size();
		
		for(DataPointType type : types)
		{
			double value = 0.0;
			
			for(int i=0; i<nValues; i++)
			{
				DataPoint forecast_dp = forecastValues.get(i);
				DataPoint actual_dp = actualValues.get(i);
				
				value += Math.pow((forecast_dp.get(type) - actual_dp.get(type)), 2);
				
				if(Double.isNaN(value))
				{
					break;
				}
			}
			
			value = Math.sqrt(value / nValues);
			rmse.put(type, value);
		}
		
		return rmse;
	}
}
