package de.invision.rub.forecasting.visualization.jfreechart;

import java.util.Date;
import java.util.TimeZone;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import de.invision.rub.forecasting.data.DataPoint;
import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The {@code JFCMapper} maps our data structure (with {@link TimeSeriesDataset} 
 * and {@link DataPoint}) to JFreeChart's data structure. 
 */
public class JFCMapper 
{		
	/**
	 * Maps a single time series' data for the specified type.
	 * @param timeseries the time series to be mapped.
	 * @param type the type for which the data shall be mapped.
	 * @return the mapped data set.
	 * @see DataPointType 
	 */
	public XYDataset mapDataset(TimeSeriesDataset timeseries, DataPointType type)
	{	
		final TimeSeriesCollection dataset = new TimeSeriesCollection();		
		final TimeSeries ts = new TimeSeries("times series data", Minute.class);		
		
		for (DataPoint datapoint : timeseries)    
		{
			RegularTimePeriod time = mapTime(datapoint.getTimestamp());
			ts.add(time, datapoint.get(type));					
		}
	
		dataset.addSeries(ts);
		return dataset;
	}
	
	/**
	 * Maps two time series' data sets for the specified type.
	 * @param actualData the original time series.
	 * @param forecastData the predicted time series.
	 * @param type the type for which the data shall be mapped.
	 * @return the mapped data set.
	 * @see DataPointType 
	 */
	public XYDataset mapDatasets(TimeSeriesDataset actualData, 
			TimeSeriesDataset forecastData, DataPointType type)
	{
		final TimeSeriesCollection dataset = new TimeSeriesCollection();		
		final TimeSeries actual_ts = new TimeSeries("actual data", Minute.class);
		final TimeSeries forecast_ts = new TimeSeries("forecast data", Minute.class);
		
		for (DataPoint dp : actualData)    
		{
			RegularTimePeriod time = mapTime(dp.getTimestamp());
			actual_ts.add(time, dp.get(type));					
		}
		for (DataPoint dp : forecastData)    
		{
			RegularTimePeriod time = mapTime(dp.getTimestamp());
			forecast_ts.add(time, dp.get(type));					
		}

		dataset.addSeries(forecast_ts);
		dataset.addSeries(actual_ts);		
		return dataset;
	}
	
	/**
	 * Converts an instance of {@code Date} to an instance of {@code Minute}.
	 * @param date the date to be converted.
	 * @return the converted date, as an instance of {@code Minute}.
	 */
	private RegularTimePeriod mapTime(Date date)
	{		
		return new Minute(date, TimeZone.getTimeZone("UTC"));
	}
}
