package de.invision.rub.forecasting;

import de.invision.rub.forecasting.algorithms.arima.ARIMA;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.input.ReaderFacade;
import de.invision.rub.forecasting.visualization.Visualizer;
import de.invision.rub.forecasting.visualization.jfreechart.JFCGraph;

/**
 * This program is the result of a Master's Degree Project at the Ruhr-Universit�t Bochum,
 * in collaboration with (and by order of) the InVision AG in D�sseldorf. Its goal was to
 * create a Java program that allows to generically add different forecasting algorithms,
 * and to implement at least one such forecasting algorithm. The optimization of this algorithm
 * was the most crucial part of the task. Representative performance measures had to be added 
 * for this purpose. Furthermore the data was sought to be visualizable in an appropriate way.
 * 
 * @author Marco Strieder 
 * @author H�lya Avsar
 * @author Azadeh Ebrahimpour
 * @version 24.07.2016
 */
public class MainUI 
{
	/**
	 * The obligatory main method, starts the program.
	 * @param args Java's command-line arguments.
	 */
	public static void main(String args[])
	{
		/* import data */
		String filepath = FileChooser.getFilepath();
		TimeSeriesDataset tsd = ReaderFacade.readData(filepath);
		
		/* if data has been successfully imported */
		if(tsd != null)
		{
			/* apply ARIMA forecasting */
		    int horizon = 35;
		    ARIMA arima = new ARIMA(args);
		    TimeSeriesDataset fc = Executer.execute(tsd, arima, horizon);
		    
		    /* visualize results */
		    Visualizer g = new JFCGraph();
		    g.plotBothData(tsd, fc);
		}
		
	    System.out.println("\nENDE DES PROGRAMMS!");
	}
}
