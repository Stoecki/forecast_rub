package de.invision.rub.forecasting.utility;

import java.util.Arrays;

/**
 * The {@code MathHelper} provides useful methods for
 * working with mathematics.
 */
public class MathHelper
{
	/**
	 * Computes the mean of the specified array.
	 * @param array the array for which the mean shall be computed.
	 * @return the specified array's mean. 
	 */
	public static double getMean(Double[] array)
	{
		double sum = 0.0;
		
		for(int i=0; i<array.length; i++)
		{
			sum += array[i];
		}
		
		return sum / array.length;
	}
	
	/**
	 * Computes the median of the specified array.
	 * @param array the array for which the median shall be computed.
	 * @return the specified array's median. 
	 */
	public static double getMedian(Double[] array)
	{
		Arrays.sort(array);
		
		if (array.length % 2 == 0)
		{
		    return (array[array.length/2] + array[array.length/2 - 1]) / 2.0;
		}
		else
		{
		    return array[array.length/2];
		}
	}
	
	/**
	 * Transforms absolute values to relative (percental) values.
	 * @param values the values to be transformed.
	 * @return the transformed values.
	 */
	public static Double[][] toPercentage(Double[][] values)
	{
		int J = values.length;
		int K = values[0].length;
		Double[][] percentages = new Double[J][K];
		Double[] sum = new Double[J];
		
		for(int j=0; j<J; j++)
		{
			sum[j] = 0.0;
			for(int k=0; k<K; k++)
			{
				sum[j] += values[j][k];
			}
			
			for(int k=0; k<K; k++)
			{
				if(sum[j] == 0.0) 
				{
					percentages[j][k] = 0.0;
				}
				else
				{
					percentages[j][k] = values[j][k] / sum[j];
				}
			}
		}
		
		return percentages;
	}
}
