package de.invision.rub.forecasting.utility;

import java.util.Scanner;

/**
 * The {@code ConsoleHelper} provides useful methods for
 * reading values from the console.
 */
public class ConsoleHelper
{
	/** 
	 * Reads an integer from the console.
	 * @return the read integer.
	 */
	public static int readInt()
	{
		Scanner s = new Scanner(System.in);
		int c = s.nextInt();
		s.close();
		System.out.println();
		return c;
	}
}
