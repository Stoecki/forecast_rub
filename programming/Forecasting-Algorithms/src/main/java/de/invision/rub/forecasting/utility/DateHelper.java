package de.invision.rub.forecasting.utility;

import java.util.Date;

/**
 * The {@code DateHelper} provides useful methods for
 * working with Java's deprecated type {@link Date}.
 */
public class DateHelper
{
	/**
	 * Computes the time between two dates.
	 * @param dt1 the first date.
	 * @param dt2 the second date.
	 * @return the time between the dates in milliseconds.
	 */
	public static long timeBetween(Date dt1, Date dt2)
	{
		return Math.abs(dt1.getTime() - dt2.getTime());
	}
	
	/**
	 * Adds a certain amount of time to a date.
	 * @param dt the date to be increased.
	 * @param ms the time to be added in milliseconds.
	 * @return the changed date. 
	 */
	public static Date plus(Date dt, long ms)
	{
		return new Date(dt.getTime() + ms);
	}
	
	/**
	 * Shifts a date by a specified multiple of a certain 
	 * amount of time.
	 * @param dt the date to be shifted.
	 * @param ms the time to be added in milliseconds.
	 * @param n the number of how often the time shall be added.
	 * @return the shifted date.  
	 */
	public static Date shift(Date dt, long ms, int n)
	{
		return plus(dt, ms*n);
	}
}
