package de.invision.rub.forecasting.input;

import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The interface {@code DataReader} defines the outer appearance 
 * for all data import methods to be provided. By doing so,
 * all classes implementing the interface can be used 
 * interchangeably, without the need of caring about their 
 * inner structures. 
 */
public interface DataReader 
{	
	/**
	 * Imports a time series from the specified file.
	 * @param filepath the path to the file to be read.
	 * @return the imported time series.
	 */
	public TimeSeriesDataset readData(String filepath);
}
