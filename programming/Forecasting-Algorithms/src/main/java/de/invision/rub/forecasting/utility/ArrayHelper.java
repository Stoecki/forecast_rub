package de.invision.rub.forecasting.utility;

/**
 * The {@code ArrayHelper} provides useful methods for
 * working with arrays.
 */
public class ArrayHelper
{
	/**
	 * Checks whether a data set consists only of zeros.
	 * @param data the data set to be checked.
	 * @return true if all values in {@code data} are zero.
	 */
	public static boolean onlyZeros(Double[] data)
	{
		for(double d : data)
		{
			if(d != 0.0)
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Merges two double arrays.
	 * @param a the first array.
	 * @param b the second array.
	 * @return the merged array.
	 */
	public static Double[] mergeArrays(Double[] a, Double[] b)
	{
		Double[] c = new Double[a.length + b.length];		
		for(int i=0; i<a.length; i++)
		{
			c[i] = a[i];
		}
		for(int i=0; i<b.length; i++)
		{
			c[a.length + i] = b[i];
		}	
		return c;
	}
	
	/**
	 * Converts data from an one-dimensional to a three-dimensional array.
	 * @param data the array to be converted.
	 * @param I the first dimension's size.
	 * @param J the second dimension's size.
	 * @param K the third dimension's size.
	 * @return the converted array. 
	 */
	public static Double[][][] from1DTo3D(Double[] data, int I, int J, int K)
	{
		Double[][][] splitted = new Double[I][J][K];
	
		if(I*J*K < data.length)
		{
			System.err.println("Couldn't convert from 1-dimensional to 3-dimensional array!");
			return splitted;
		}
		
		for(int i=0; i<I; i++)
		{
			for(int j=0; j<J; j++)
			{
				for(int k=0; k<K; k++)
				{	
					/* if the index is out of bounds */
					if(i*J*K + j*K + k >= data.length)
					{
						splitted[i][j][k] = 0.0;						
					}
					else
					{
						splitted[i][j][k] = data[i*J*K + j*K + k];
					}
				}
			}
		}
		return splitted;
	}
}
