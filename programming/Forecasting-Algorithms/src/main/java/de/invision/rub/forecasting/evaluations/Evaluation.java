package de.invision.rub.forecasting.evaluations;

import java.util.Map;

import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The interface {@code Evaluation} defines the outer appearance 
 * for all evaluation methods to be provided. By doing so,
 * all classes implementing the interface can be used 
 * interchangeably, without the need of caring about their 
 * inner structures. 
 */
public interface Evaluation 
{
	/**
	 * Evaluates a forecasting's results by comparing 
	 * the predicted values to the actual values.
	 * @param actualValues the original data.
	 * @param forecastValues the predicted data.
	 * @return a map of all evaluation results and their corresponding types.
	 * @see DataPointType 
	 */
	public Map<DataPointType,Double> evaluate(TimeSeriesDataset actualValues, TimeSeriesDataset forecastValues);
}
