package de.invision.rub.forecasting.data;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/** 
 * The class {@code DataPoint} represents a single moment within a time series.
 * It consists of a unique timestamp, corresponding to the time series' 
 * time interval and the data point's position within that series, 
 * and arbitrarily many values for (distinct and pre-defined) 
 * types, e.g. "offered calls" or "handling time" in the context 
 * of a call center.
 * 
 * <br><br>Instances of this class are comparable by means of their timestamp 
 * (which consists of a date and time, e.g. {@code 2016-07-24 12:30:00}), 
 * so that a set of data points can be ordered, if required.
 * @see TimeSeriesDataset
 * @see DataPointType
 */
public class DataPoint implements Comparable<DataPoint>, Iterable<Double>
{
	private Date timestamp;
	private Map<DataPointType, Double> values = new HashMap<DataPointType, Double>();
	
	/** 
	 * Class constructor. 
	 */
	public DataPoint()
	{
		// empty constructor
	}
	
	/** 
	 * Class constructor specifying the data point's timestamp.
	 * @param timestamp the data point's timestamp as an instance of {@code Date}. 
	 */
	public DataPoint(Date timestamp)
	{
		this.timestamp = timestamp;
	}
	
	/** 
	 * Class constructor specifying the data point's set of values. 
	 * @param values a map of values, identified respectively by their type.
	 * @see DataPointType
	 */
	public DataPoint(Map<DataPointType, Double> values)
	{
		this.values.putAll(values);
	}
	
	/** 
	 * Class constructor specifying the data point's timestamp and
	 * set of values. 
	 * @param timestamp the data point's timestamp as an instance of {@code Date}.
	 * @param values a map of values, identified respectively by their type.
	 * @see DataPointType
	 */
	public DataPoint(Date timestamp, Map<DataPointType, Double> values)
	{
		this.timestamp = timestamp;
		this.values.putAll(values);
	}
	
	/**
	 * Getter method.
	 * @return the data point's timestamp.
	 */
	public Date getTimestamp() 
	{
		return timestamp;
	}

	/**
	 * Setter method.
	 * @param timestamp the data point's timestamp.
	 */
	public void setTimestamp(Date timestamp) 
	{
		this.timestamp = timestamp;
	}
	
	/**
	 * Setter method.
	 * @param values a map of values, identified respectively by their type.
	 * @see DataPointType
	 */
	public void setValues(Map<DataPointType, Double> values)
	{
		this.values.putAll(values);
	}
	
	/**
	 * Adds a value (or changes the value) for the specified type.
	 * @param type the type of the value to be set.
	 * @param value the value to be set.
	 * @see DataPointType
	 */
	public void set(DataPointType type, double value)
	{
		values.put(type, value);
	}
	
	/** 
	 * Getter method.
	 * @param type the type for which the value shall be returned.
	 * @return the value of the specified type. 
	 * @see DataPointType
	 */
	public double get(DataPointType type)
	{
		if(values.containsKey(type))
		{
			return values.get(type);
		}
		return -1.0;		
	}
	
	/** 
	 * Getter method.
	 * @return a set of all types which are managed by this data point.
	 * @see DataPointType
	 */
	public DataPointType[] getTypes()
	{
		DataPointType[] typesArray = new DataPointType[values.size()];
		int i = 0;
		for(DataPointType type : values.keySet())
		{
			typesArray[i++] = type;
		}
		return typesArray; 
	}	

	/**
	 * Compares two data points by means of their timestamps.
	 * @param datapoint the data point to be compared.
	 * @return a negative integer, zero, or a positive integer as this 
	 * data point's timestamp is less than, equal to, or greater than 
	 * the specified data point's timestamp. 
	 */
	@Override
	public int compareTo(DataPoint datapoint) 
	{
		return timestamp.compareTo(datapoint.getTimestamp());		
	}
	
	/**
	 * Iterator method.
	 * @return an iterator for accessing all double values contained 
	 * in this data point.
	 */
	@Override
    public Iterator<Double> iterator() 
	{		       
        Iterator<Double> iterator = new Iterator<Double>() 
        {   
        	private int index = 0;
        	private DataPointType[] types = getTypes();
        	
            @Override
            public boolean hasNext() 
            {
                return index < types.length;
            }

            @Override
            public Double next() 
            {
            	return values.get(types[index++]);
            }

            @Override
            public void remove() 
            {
                throw new UnsupportedOperationException();
            }
        };
        return iterator;
    }
}
