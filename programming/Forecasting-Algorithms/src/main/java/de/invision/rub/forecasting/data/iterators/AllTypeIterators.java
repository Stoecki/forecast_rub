package de.invision.rub.forecasting.data.iterators;

import java.util.ArrayList;

import de.invision.rub.forecasting.data.DataPointType;

/**
 * {@code AllTypeIterators} provides a list of all type iterators.
 * @see TypeIterator
 */
@SuppressWarnings("serial")
public class AllTypeIterators extends ArrayList<TypeIterator>
{
	/**
	 * Adds a {@link TypeIterator} for every specified type.
	 * @param types all types that shall be handled.
	 * @see DataPointType 
	 */
	public void initialize(DataPointType[] types)
	{
		clear();
		for(DataPointType type : types)
		{
			add(new TypeIterator(type));
		}
	}
}
