package de.invision.rub.forecasting;

import java.util.Map;
import java.util.function.Function;
import de.invision.rub.forecasting.algorithms.Algorithm;
import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.evaluations.mape.MAPE;
import de.invision.rub.forecasting.evaluations.rmse.RMSE;

/**
 * The {@code Executer} lightens the {@code MainUI}'s workload (and 
 * contributes both to the principles of secrecy and coupling/cohesion) 
 * by outsourcing instructions which are not the UI's business.
 */
public class Executer
{
	/**
	 * Partitions the example data into training data and test data,
	 * executes the forecasting algorithm with the training data, then
	 * assesses the results by using the test data and applying
	 * evaluation methods.
	 * @param timeseries the time series to be forecast.
	 * @param algorithm the forecasting algorithm to be applied.
	 * @param horizon the horizon to be used within the forecasting.
	 * @return a time series containing the predicted future values.
	 */
	public static TimeSeriesDataset execute(TimeSeriesDataset timeseries, 
			Algorithm algorithm, int horizon)
	{
		/* partitioning data into training data and test data */
		int nWeeks = timeseries.size() / timeseries.getValuesPerDay();		
		int nTraining = ((int)Math.floor(nWeeks * 0.8) / 7) * 7 * timeseries.getValuesPerDay();
		int nTest = timeseries.size() - nTraining;
		
		/* there shouldn't be more forecast data than test data */
		if((horizon * timeseries.getValuesPerDay()) > nTest)
		{
			horizon = nTest / timeseries.getValuesPerDay();
		}		
		System.out.println("Number of data points: " + timeseries.size());
		System.out.println("Number of training data points: " + nTraining);
		System.out.println("Number of test data points: " + nTest);
		System.out.println("Number of forecast data points: " + 
				(horizon * timeseries.getValuesPerDay()) + "\n");
		
		/* dividing test data and training data */
		TimeSeriesDataset trainingData = TimeSeriesDataset.firstValues(timeseries, nTraining);
		TimeSeriesDataset testData = TimeSeriesDataset.lastValues(timeseries, nTest);
		
		/* forecasting */
		TimeSeriesDataset forecastData = algorithm.forecast(trainingData, horizon);
		
		/* evaluation */
		int n = horizon * timeseries.getValuesPerDay();		
		TimeSeriesDataset actualValues = TimeSeriesDataset.firstValues(testData, n);
		TimeSeriesDataset forecastValues = TimeSeriesDataset.lastValues(forecastData, n);		
		applyEvaluations(actualValues, forecastValues);
		
		return forecastValues;
	}
	
	/**
	 * Applies different evaluation techniques to the forecasting's result data
	 * to estimate the results' quality and the algorithm's effectiveness.
	 * @param actualValues the original values.
	 * @param forecastValues the predicted values.
	 */
	private static void applyEvaluations(TimeSeriesDataset actualValues, TimeSeriesDataset forecastValues)
	{	
		/* converts double values into percentage strings */
		Function<Double, String> percentageString = (value) -> {
			return ((int) Math.round(value * 10000)) / 100.0 + "%";
		};
		
		RMSE rmse = new RMSE();
		Map<DataPointType,Double> rmseErrors = rmse.evaluate(actualValues, forecastValues);		
		System.out.println("RMSE for OFFERED_CALLS = " + rmseErrors.get(DataPointType.OFFERED_CALLS));
		System.out.println("RMSE for HANDLED_CALLS = " + rmseErrors.get(DataPointType.HANDLED_CALLS));
		System.out.println("RMSE for HANDLING_TIME = " + rmseErrors.get(DataPointType.HANDLING_TIME));
		
		System.out.println();
		
		MAPE mape = new MAPE();
		Map<DataPointType,Double> mapeErrors = mape.evaluate(actualValues, forecastValues);		
		System.out.println("MAPE for OFFERED_CALLS = " + percentageString.apply(mapeErrors.get(DataPointType.OFFERED_CALLS)));
		System.out.println("MAPE for HANDLED_CALLS = " + percentageString.apply(mapeErrors.get(DataPointType.HANDLED_CALLS)));
		System.out.println("MAPE for HANDLING_TIME = " + percentageString.apply(mapeErrors.get(DataPointType.HANDLING_TIME)));
	}
}
