package de.invision.rub.forecasting.algorithms.arima;

import de.invision.rub.forecasting.utility.ArrayHelper;
import de.invision.rub.forecasting.utility.MathHelper;

/**
 * The class {@code Patterns} provides methods for deriving 
 * patterns from data series. So far the only implemented one
 * determines a pattern for each day of the week. 
 */
public class Patterns
{
	/**
	 * Derives a pattern for each days of the week.
	 * @param timeseries the time series to be the basis of derivation.
	 * @param valuesPerDay the number of values per day within the data.
	 * @return a two-dimensional array of all patterns, where the first dimension
	 * stands for the day of the week and the second dimension for the pattern's
	 * percental values.
	 */
	public static Double[][] getDaysOfWeekPattern(Double[] timeseries, int valuesPerDay)
	{
		return getPattern(timeseries, valuesPerDay, 7);
	}
	
	/**
	 * Derives pattern in the specified amount and size.
	 * @param timeseries the time series to be the basis of derivation.
	 * @param lengthOfPattern the number of values per pattern.
	 * @param numberOfPatterns the number of patterns to be derived.
	 * @return a two-dimensional array of all patterns, where the first dimension
	 * stands for the pattern and the second dimension for the pattern's
	 * percental values.
	 */
	private static Double[][] getPattern(Double[] timeseries, int lengthOfPattern, int numberOfPatterns)
	{	
		int numberOfOccurances = (int) Math.ceil(timeseries.length / (numberOfPatterns * lengthOfPattern));
				
		/* simplify index set names */
		int I = numberOfOccurances, J = numberOfPatterns, K = lengthOfPattern;
		
		/* convert data from one-dimensional to three-dimensional */
		Double[][][] splittedData = ArrayHelper.from1DTo3D(timeseries, I, J, K);
		Double[][] resultData = new Double[J][K];
		
		/* for all patterns */
		for(int j=0; j<J; j++)
		{
			Double[] mean = new Double[K];	
			
			/* for all values of the pattern */
			for(int k=0; k<K; k++)
			{
				Double[] timepointData = new Double[I];				
				
				/* for all values of this timepoint */
				for(int i=0; i<I; i++)
				{
					timepointData[i] = splittedData[i][j][k];
				}								
				
				/* compute the timepoint's median */
				mean[k] = MathHelper.getMedian(timepointData);				
			}			
			resultData[j] = mean;
		}
		
		/* transform absolute to relative (percental) values */
		Double[][] patterns = MathHelper.toPercentage(resultData);
		return patterns;
	}
}
