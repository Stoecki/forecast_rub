package de.invision.rub.forecasting.visualization;

import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The interface {@code Visualizer} defines the outer appearance 
 * for all visualization methods to be provided. By doing so,
 * all classes implementing the interface can be used 
 * interchangeably, without the need of caring about their 
 * inner structures. 
 */
public interface Visualizer 
{	
	/**
	 * Visualizes a time series.
	 * @param timeseries the time series to be visualized.
	 */
	public void plotData(TimeSeriesDataset timeseries);
	
	/**
	 * Visualizes two time series.
	 * @param actualData the original time series.
	 * @param forecastData the predicted time series.
	 */
	public void plotBothData(TimeSeriesDataset actualData, TimeSeriesDataset forecastData);
}
