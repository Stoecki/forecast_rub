package de.invision.rub.forecasting.algorithms;

import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The interface {@code Algorithm} defines the outer appearance 
 * for all forecasting algorithms to be provided. By doing so,
 * all algorithms implementing the interface can be used 
 * interchangeably, without the need of caring about the algorithms' 
 * inner structures. 
 */
public interface Algorithm 
{
	/**
	 * Uses the data from the specified time series to predict future
	 * values, in the amount of the given horizon.
	 * @param timeseries the training data set for the forecasting.
	 * @param horizon the number of predictions to be made.
	 * @return a time series containing the predicted future values.
	 */
	public TimeSeriesDataset forecast(TimeSeriesDataset timeseries, int horizon);
}
