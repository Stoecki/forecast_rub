package de.invision.rub.forecasting.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import de.invision.rub.forecasting.data.iterators.AllTypeIterators;
import de.invision.rub.forecasting.data.iterators.ArrayIterator;
import de.invision.rub.forecasting.data.iterators.DayValueIterator;
import de.invision.rub.forecasting.utility.DateHelper;

/**
 * The class {@code TimeSeriesDataset} is an extended {@link ArrayList},
 * collecting instances of {@link DataPoint} and providing additional 
 * functionalities. 
 * @see DataPoint
 */
public class TimeSeriesDataset extends ArrayList<DataPoint>
{
	private static final long serialVersionUID = 3770905641720384980L;
	private static final long MILLISECONDS_PER_DAY = 86400000;
	
	private Date timeStart;
	private long timeInterval; // in milliseconds
	private int valuesPerDay;
	
	/**
	 * Constructor. 
	 */
	public TimeSeriesDataset()
	{
		// empty constructor
	}
	
	/**
	 * Constructor specyfing the time series' first timestamp
	 * and the timestamps' interval. It automatically computes
	 * the number of values per day.
	 * @param timeStart the series' first timestamp.
	 * @param timeInterval the series' time interval in milliseconds. 
	 */
	public TimeSeriesDataset(Date timeStart, long timeInterval)
	{
		this.timeStart = timeStart;
		this.timeInterval = timeInterval;		
		this.valuesPerDay = (int) (MILLISECONDS_PER_DAY / timeInterval);
	}
	
	/**
	 * Setter method.
	 * @param timeStart the series' first timestamp.
	 */
	public void setTimeStart(Date timeStart)
	{
		this.timeStart = timeStart;
	}
	
	/**
	 * Setter method. It automatically computes
	 * the number of values per day.
	 * @param timeStart the series' first timestamp.
	 * @param timeInterval the series' time interval in milliseconds.
	 */
	public void setTimes(Date timeStart, long timeInterval)
	{
		this.timeStart = timeStart;
		this.timeInterval = timeInterval;
		this.valuesPerDay = (int) (MILLISECONDS_PER_DAY / timeInterval);
	}
	
	/**
	 * Getter method.
	 * @return the number of values per day. 
	 */
	public int getValuesPerDay()
	{
		return valuesPerDay;
	}
	
	/**
	 * Getter method.
	 * @return a set of all value types managed within this time series.
	 * @see DataPointType 
	 */
	public DataPointType[] getTypes()
	{
		if(!this.isEmpty())
		{
			return get(0).getTypes();
		}
		return null;
	}
	
	/**
	 * Getter method.
	 * @return the series' first timestamp. 
	 */
	public Date getTimeStart()
	{
		return timeStart;
	}
	
	/**
	 * Getter method.
	 * @return the series' time interval in milliseconds.
	 */
	public long getTimeInterval()
	{
		return timeInterval;
	}
	
	/**
	 * Adds a data point to this time series.
	 * @param datapoint the data point to be added.
	 * @return true if the data point was added successfully.
	 */
	@Override
	public boolean add(DataPoint datapoint)
	{
		return super.add(datapoint);		
	}
	
	/**
	 * Transforms raw data into an instance of {@code TimeSeriesDataset}.
	 * @param data the data to be stored in the time series, as a map of 
	 * double arrays and their corresponding types.
	 * @param size the number of data points to be created.
	 * @param timeStart the series' first timestamp.
	 * @param timeInterval the series' time interval in milliseconds.
	 * @return the newly created time series.
	 * @see DataPointType
	 */
	public static TimeSeriesDataset from(Map<DataPointType,Double[]> data, int size,
			Date timeStart, long timeInterval)
	{
		TimeSeriesDataset timeseries = new TimeSeriesDataset(timeStart, timeInterval);
		
		/* for all data points */
		for(int i=0; i<size; i++)
		{
			DataPoint datapoint = new DataPoint();			
			
			/* for all data types */
			for(DataPointType type : data.keySet())
			{
				datapoint.set(type, data.get(type)[i]);
			}
			timeseries.add(datapoint);
		}
		
		return timeseries;
	}
	
	/**
	 * String method.
	 * @return a string representation of this time series.
	 */
	@Override
	public String toString()
	{
		String string = "";
		for(DataPoint datapoint : this)
		{
			string += datapoint.toString() + "\n";
		}
		return string;
	}
	
	/* ITERATOR METHODS */
	
	/**
	 * Iterator method.
	 * @return an iterator for accessing all data points contained
	 * in this time series.
	 */
	@Override
    public Iterator<DataPoint> iterator() 
	{		
        int size = this.size();        
        Iterator<DataPoint> iterator = new Iterator<DataPoint>() 
        {   
        	private int index = 0;
        	private Date timestamp = timeStart;
        	
            @Override
            public boolean hasNext() 
            {
                return index < size && get(index) != null;
            }

            @Override
            public DataPoint next() 
            {
            	DataPoint datapoint = get(index++);
            	datapoint.setTimestamp(timestamp);
            	timestamp = DateHelper.plus(timestamp, timeInterval);
                return datapoint;
            }

            @Override
            public void remove() 
            {
                throw new UnsupportedOperationException();
            }
        };
        return iterator;
    }
	
	/**
	 * Iterator method.
	 * @return an iterator for accessing all double values contained
	 * in this time series.
	 */
	public Iterator<Double[]> getArrayIterator()
	{
		ArrayIterator iterator = new ArrayIterator();
		iterator.initialize(this);
		return iterator;
	}
	
	/**
	 * Iterator method.
	 * @return a list of iterators for iterating respectively over all types
	 * contained in the time series.
	 */
	public AllTypeIterators getTypeIterators()
	{
		AllTypeIterators iterators = new AllTypeIterators();
		iterators.initialize(this.getTypes());
		return iterators;
	}
	
	/**
	 * Iterator method.
	 * @return an iterator for accessing all day values (sum of all values per day)
	 * contained in this time series.
	 */
	public Iterator<Double[]> getDayValueIterator()
	{
		DayValueIterator iterator = new DayValueIterator();
		iterator.initialize(this, valuesPerDay);
		return iterator;
	}
	
	/* STATIC METHODS */
	
	/**
	 * Returns the first {@code n} data points from the specified time series.
	 * @param timeseries the time series to be cropped.
	 * @param n the number of data points to be retained.
	 * @return a time series containing only the specified data points. 
	 */
	public static TimeSeriesDataset firstValues(TimeSeriesDataset timeseries, int n)
	{
		TimeSeriesDataset result = new TimeSeriesDataset(
				timeseries.getTimeStart(), timeseries.getTimeInterval());
		
		if(n > timeseries.size())
		{
			return result;
		}
		
		int counter = 0;
		
		for(DataPoint datapoint : timeseries)
		{
			if(++counter > n)
			{
				break;
			}
			result.add(datapoint);
		}
				
		return result;
	}
	
	/**
	 * Returns the last {@code n} data points from the specified time series.
	 * @param timeseries the time series to be cropped.
	 * @param n the number of data points to be retained.
	 * @return a time series containing only the specified data points. 
	 */
	public static TimeSeriesDataset lastValues(TimeSeriesDataset timeseries, int n)
	{
		TimeSeriesDataset result = new TimeSeriesDataset(
				timeseries.getTimeStart(), timeseries.getTimeInterval());
		
		if(n > timeseries.size())
		{
			return result;
		}
		
		n = timeseries.size() - n + 1;
		result.setTimeStart(DateHelper.shift(result.getTimeStart(), result.getTimeInterval(), n));
		
		int counter = 0;
		
		for(DataPoint datapoint : timeseries)
		{
			if(++counter < n)
			{
				continue;
			}
			result.add(datapoint);
		}
		
		return result;
	}
	
	/**
	 * Cuts off the last {@code n} data points from the specified time series.
	 * @param timeseries the time series to be cropped.
	 * @param n the number of data points to be removed.
	 * @return a time series missing the specified data points. 
	 */
	public static TimeSeriesDataset cutOff(TimeSeriesDataset timeseries, int n)
	{
		if(timeseries.size() > n)
		{
			return firstValues(timeseries, timeseries.size() - n);
		}
		else
		{
			System.err.println("Cannot cut off " + n + " data points from this time series, "
					+ "since it contains less than " + (n+1) + " data points.");
			return timeseries;
		}
	}
}
