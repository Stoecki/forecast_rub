package de.invision.rub.forecasting.evaluations.mape;

import java.util.HashMap;
import java.util.Map;

import de.invision.rub.forecasting.data.DataPoint;
import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.evaluations.Evaluation;

/** 
 * The class {@code MAPE} computes the "mean absolute percentage error" (MAPE). 
 */
public class MAPE implements Evaluation
{
	@Override
	public Map<DataPointType, Double> evaluate(TimeSeriesDataset actualValues, TimeSeriesDataset forecastValues)
	{
		Map<DataPointType, Double> mape = new HashMap<DataPointType, Double>();
		
		DataPointType[] types = actualValues.getTypes();
		
		int nValues = actualValues.size();
		
		for(DataPointType type : types)
		{
			double value = 0.0;
			
			for(int i=0; i<nValues; i++)
			{
				DataPoint forecast_dp = forecastValues.get(i);
				DataPoint actual_dp = actualValues.get(i);
				
				if(actual_dp.get(type) == 0.0)
				{
					value += forecast_dp.get(type);
				}
				else
				{
					value += Math.abs((actual_dp.get(type) - forecast_dp.get(type)) / actual_dp.get(type));
				}					
			}
			
			value /= nValues;
			mape.put(type, value);
		}
		
		return mape;
	}
}
