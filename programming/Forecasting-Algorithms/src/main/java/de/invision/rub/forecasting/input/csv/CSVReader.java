package de.invision.rub.forecasting.input.csv;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import de.invision.rub.forecasting.data.DataPoint;
import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.input.DataReader;
import de.invision.rub.forecasting.utility.DateHelper;

/**
 * The {@code CSVReader} imports data from a csv file.
 */
public class CSVReader implements DataReader
{		
	@Override
	@SuppressWarnings("deprecation")
	public TimeSeriesDataset readData(String filepath)
	{
		System.out.println("Importing data from file: \n" + filepath + "\n");
		
		TimeSeriesDataset tsd = new TimeSeriesDataset();
		Date date1 = null, date2 = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
		
		try
		{
			/* read data as string from CSV file */
			String csvString = new String(Files.readAllBytes(Paths.get(filepath)));	
			
			/* skip the first row */
			csvString = csvString.substring(csvString.indexOf('\n')+1);
			
			/* helpers for the parsing */
			boolean isHeader = true;
			Map<Integer, DataPointType> types = new HashMap<Integer, DataPointType>();
			
			/* parse string into CSV records */
			CSVParser parser = CSVParser.parse(csvString, CSVFormat.RFC4180);
			for(CSVRecord csvRecord : parser) 
			{	
				/* if it's the header row */
				if(isHeader)
				{
					int i = 0;
					for(String s : csvRecord)
					{
						if(i != 0)
						{						
							types.put(i, DataPointType.valueOf(s.toUpperCase()));
						}
						i++;
					}
					isHeader = false;
					continue;
				}
				
				/* parse date string */
				String datetime = csvRecord.get(0).trim();
				Date date =  df.parse(datetime);  
				
				/* the date has to be corrected by one hour */
				date.setTime(date.getTime() + date.getTimezoneOffset() * 60 * 1000); 				
				
				/* store first two dates for determining the starting time and time interval */
				if (date1 == null)
				{
					date1 = date;
				}
				else if (date2 == null)
				{
					date2 = date;
				}
				
				/* add the new data point to the time series dataset */
				Map<DataPointType, Double> values = new HashMap<DataPointType, Double>();
				for(int i=1; i<csvRecord.size(); i++)
				{
					values.put(types.get(i), Double.parseDouble(csvRecord.get(i).trim()));
				}
				tsd.add(new DataPoint(date, values));
			}			
			tsd.setTimes(date1, DateHelper.timeBetween(date1, date2));			
			
			/* make sure that only complete weeks are used */
			if(tsd.size() % 7 != 0)
			{
				int n = tsd.size() - 7 * (tsd.size() / 7);
				tsd = TimeSeriesDataset.cutOff(tsd, n);
			}
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
		
		return tsd;
	}
}
