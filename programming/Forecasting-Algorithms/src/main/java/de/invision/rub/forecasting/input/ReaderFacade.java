package de.invision.rub.forecasting.input;

import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.input.csv.CSVReader;
import org.apache.commons.io.FilenameUtils;

/**
 * The {@code ReaderFacade} implements the facade pattern to hide 
 * the actual implementations of the interface {@code DataReader}.
 */
public class ReaderFacade 
{
	private static DataReader csvReader = new CSVReader();
	
	/**
	 * Extracts the file's extension and uses it to determine which
	 * implementation of {@code DataReader} shall be used.
	 * @param filepath the path to the file to be read.
	 * @return the imported time series.
	 */
	public static TimeSeriesDataset readData(String filepath)
	{
		String extension = FilenameUtils.getExtension(filepath);
		
		switch (extension)
		{
			case "csv": 
			{
				return csvReader.readData(filepath);
			}
			default: 
			{
				System.err.println("Dieses Dateiformat wird nicht unterstützt.");
				return null;
			}
		}
	}

}
