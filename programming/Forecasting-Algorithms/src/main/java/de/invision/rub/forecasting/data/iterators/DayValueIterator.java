package de.invision.rub.forecasting.data.iterators;

import java.util.ArrayList;
import java.util.Iterator;
import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The {@code DayValueIterator} accumulates a 
 * time series' values to daily values. 
 */
public class DayValueIterator implements Iterator<Double[]>
{
	ArrayList<Double[]> arrays;
	int index;
	
	/**
	 * Initializes the iterator with a specific time series
	 * and its corresponding number of values per day.
	 * @param timeseries the time series to be used by the iterator.
	 * @param valuesPerDay the number of values per day in the 
	 * specified time series.
	 */
	public void initialize(TimeSeriesDataset timeseries, int valuesPerDay)
	{
		index = 0;
		Double[] array;		
		arrays = new ArrayList<Double[]>();
		int counter = 0;
		
		for(TypeIterator iterator : timeseries.getTypeIterators())
		{
			int length = timeseries.size() / valuesPerDay;
			array = new Double[length+1];
			array[length] = iterator.getType().getValue();
			
			int i = 0;
			double sum = 0.0;
			iterator.init(timeseries);
			
			for(Double value : iterator)
			{
				counter++;
				if(counter == valuesPerDay)
				{
					counter = 0;
					array[i++] = sum;
					sum = 0.0;
				}
				else
				{
					sum += value;
				}
			}
			arrays.add(array);
		}
	}
	
	@Override
    public boolean hasNext() 
    {
        return index < arrays.size() && arrays.get(index) != null;
    }

    @Override
    public Double[] next() 
    {
    	return arrays.get(index++);
    }

    @Override
    public void remove() 
    {
        throw new UnsupportedOperationException();
    }	
}
