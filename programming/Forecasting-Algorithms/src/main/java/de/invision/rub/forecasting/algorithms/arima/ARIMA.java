package de.invision.rub.forecasting.algorithms.arima;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.rosuda.JRI.Rengine;
import de.invision.rub.forecasting.algorithms.Algorithm;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.utility.ArrayHelper;
import de.invision.rub.forecasting.data.DataPointType;

/**
 * The class {@code ARIMA} implements a forecasting method called "ARIMA Modeling"
 * (with the acronym ARIMA standing for "autoregressive integrated moving average").
 * It's a combination of the stochastic methods "autoregression" and "moving average", 
 * enhanced by the application of "data differencing" (which adds the 'I' to ARIMA).
 * 
 * <br><br>The ARIMA model expects three input parameters {@code p}, {@code d}, and 
 * {@code q} - where {@code p} denotes the order of the AR model, {@code d} is the number 
 * of necessary differencing iterations, and {@code q} denotes the order of the MA model. 
 * The value of {@code d} can be easily determined by differencing the data until it's
 * stationary. The data's stationarity can be calculated by using a unit root test.
 * The most common unit root test with ARIMA modeling (applied in this class) is the 
 * "Augmented Dickey-Fuller Test". The values for {@code p} and {@code q} can be specified 
 * by applying the "Box-Jenkins Method", which analyses the time series' autocorrelation 
 * plots (ACF and PACF). After deciding for a certain {@code p} and {@code q}, a gridsearch
 * is carried out for the aim of optimizing the choice of parameters.
 * 
 * <br><br>Instead of using the data directly, it is summed up to daily values beforehand, 
 * and for every day of the week a "typical pattern" is calculated. After predicting future 
 * daily values, the initial accumulation is reversed by applying the patterns to the 
 * forecasting results. In addition to that, a logarithmic transformation is employed on the
 * data. (But our tests have revealed that both, the gridsearch and the transformation, have 
 * little to no impact on the later results. 
 * 
 * <br><br>This class is using JRI, an API that bridges between Java and the programming 
 * language R, for using canned methods instead of reinventing the wheel. By utilizing an
 * instance of {@link Rengine}, R code can be executed directly within Java.
 */
public class ARIMA implements Algorithm
{
	/* additional hyperparameters */
	private final int PERIOD = 28; // the data's period size (during our tests the 
								   // value of 28 turned out to perform very well)
	private final double BIAS = 1.0; // multiply each forecast value by this bias
	private final int G = 1; // size of the grid search (G = 0 means no grid search)
	
	/* needed for using JRI */
	private String[] args;
	private Rengine re; // allows the usage of R commands within Java
	
	/* initializes ARIMA */
	public ARIMA(String[] args)
	{
		this.args = args;
	}
	
	@Override
	public TimeSeriesDataset forecast(TimeSeriesDataset timeseries, int horizon) 
	{
		Map<DataPointType,double[]> resultArrays = new HashMap<DataPointType,double[]>();
		Map<DataPointType,Double[]> arrays = new HashMap<DataPointType,Double[]>();
		
		final int VPD = timeseries.getValuesPerDay();
		
		/* start R engine */
		re = new Rengine(args, false, null);
		if (!re.waitForR()) 
        {
            System.out.println("Cannot load R");
            return null;
        }
		
		/* import needed libraries */
        //re.eval("install.packages(\"forecast\")");
        re.eval("library(forecast)");
        re.eval("library(tseries)");
		
        /* for all time series in dataset, get the daily values */
		for(Iterator<Double[]> iterator = timeseries.getDayValueIterator(); iterator.hasNext();)		
		{			
			Double[] data = iterator.next(); // retrieve the next row of data
			DataPointType type = DataPointType.get(data[data.length-1]); // type of the current data
			data = Arrays.copyOfRange(data, 0, data.length-1);	// data without the type entry		
			System.out.println("Computing ARIMA forecast for " + type.toString() + ".");
	        
			/* if the data consists only of zeros */
			if(ArrayHelper.onlyZeros(data))
			{
				/* return an empty array and continue 
				 * with the next row of data */
				double[] results = new double[horizon]; 
				resultArrays.put(type, results);
				continue;
			}
			
	        /* ARIMA parameters */
	        int p = 0, d = 0, q = 0;
	        
	        /* prepare data for ARIMA */
	        re.eval("data = " + arrayToRString(data)); // this data is used for the ARIMA modeling
	        re.eval("datacopy = " + arrayToRString(data)); // the copy is used for stationarity testing
	        
	        /* apply log transformation */
	        re.eval("data = log1p(data)"); // log1p() avoids typical problems with zeros in logarithms
	        re.eval("datacopy = log1p(datacopy)");
	        
	        /* apply Augmented Dickey-Fuller Test */	        
	        try
	        {
	        	/* if the p-value is bigger than 0.05, assume non-stationarity */
		        while(re.eval("adf.test(datacopy)$p.value").asDouble() > 0.05)
		        {
		        	/* apply data differencing */
		        	re.eval("datacopy = diff(datacopy)");
		        	d++; // for every applied differencing, increment d by one
		        }
	        }
	        /* if a problem occured during stationarity testing */
	        catch(Exception e)
	        {
	        	d = 0; // apply no differencing within ARIMA
	        }
	    
	        /* convert raw data to time series */
	        re.eval("timeseries = ts(data)");
	        
	        /* compute ACF and PACF (autocorrelation functions) */
	        int lagMax = (int)(10 * Math.log10(data.length)); 	
	        double significanceLevel = re.eval("qnorm((1+0.95)/2)/sqrt(sum(!is.na(timeseries)))").asDouble();
	        p = p(acf(lagMax), significanceLevel); // estimating p	        	        
	        q = q(pacf(lagMax), significanceLevel);	// estimating q        
	        
	        /* apply gridsearch and ARIMA modeling */	        
	        String order = gridsearchOrder(p, d, q, PERIOD, G);
	        re.eval("model = arima(timeseries, seasonal=list(order=" + order + ", period=" + PERIOD + "))");
	     	
	        /* do the forecasting and undo log transformation */
	        re.eval("fc = forecast(model, h=" + horizon + ")");
	        double[] results = re.eval("expm1(fc$mean)").asDoubleArray();	 
	        
	        /* merge original data and forecasts */	        
	        resultArrays.put(type, results);
		}
        
		/* for all time series in dataset, get the actual (not daily) values */
		for(Iterator<Double[]> iterator = timeseries.getArrayIterator(); iterator.hasNext();)		
		{						
			Double[] data = iterator.next(); // retrieve the next row of data
			DataPointType type = DataPointType.get(data[data.length-1]); // type of the current data
			data = Arrays.copyOfRange(data, 0, data.length-1);	// data without the type entry
	        
			/* determine a pattern for each day of the week */
			Double[][] patterns = Patterns.getDaysOfWeekPattern(data, VPD);
			int nextDayOfWeek = (timeseries.size() / VPD) % 7;
			
			/* reconvert forecast from daily data to timeseries */
			double[] results = resultArrays.get(type);			
			Double[] forecast = new Double[results.length * VPD];			
			int k = nextDayOfWeek;			
			for(int i=0; i<results.length; i++)
			{	
				/* k-th day of the week */
				Double[] pattern = patterns[k];
				k = ((k + 1) % 7);
				
				/* for each value per day */
				for(int j=0; j<VPD; j++)
				{
					forecast[i*VPD+j] = pattern[j] * results[i] * BIAS;
				}
			}
			
	        /* merge original data and forecasts */	        
	        arrays.put(type, ArrayHelper.mergeArrays(data, forecast));
		}
		
		/* stop R engine */
        re.end();
		
        /* convert resulting double arrays back to time series dataset */
		TimeSeriesDataset fc = TimeSeriesDataset.from(arrays, timeseries.size() + horizon * VPD, 
				timeseries.getTimeStart(), timeseries.getTimeInterval());		
		return fc;        
	}

	/* OUTSOURCED PROCEDURES */
		
	/**
	 * Applies a gridsearch to parameters {@code p} and {@code q}, with step size 
	 * {@code G} in all directions.
	 * @param P the basis for searching p.
	 * @param d the order of the data's differencing.
	 * @param Q the basis for searching q.
	 * @param period the data's period.
	 * @param G the grid's size for gridsearch.
	 * @return the combination of p, d and q as a string.
	 */
	private String gridsearchOrder(int P, int d, int Q, int period, int G)
	{	
		System.out.println("Calculated p = " + P + " and q = " + Q + ".");
		
		/* if it's impossible to go G steps to the left */
		if(P - G < 0)
		{
			P = G;
		}		
		if(Q - G < 0)
		{
			Q = G;
		}
		
		System.out.print("Testing p from " + (P-G) + " to " + (P+G));
		System.out.println(" and q from " + (Q-G) + " to " + (Q+G) + ".");
		
		/* the original ARIMA model to be optimized */
		String order = "c(" + P + "," + d + "," + Q + ")";
		re.eval("model = arima(timeseries, seasonal=list(order=" + order + ", period=" + period + "))");
     	double AIC_old = re.eval("model$aic").asDouble();
     	System.out.print("*");
     	
		/* p = ... (P-1) (P) (P+1) ... */
		for(int p = P - G; p <= P + G; p++)
		{
			/* q = ... (Q-1) (Q) (Q+1) ... */
			for(int q = Q - G; q <= Q + G; q++)
			{
				/* skip the original model */
				if(p == P && q == Q)
				{
					continue;
				}
				
				/* create new ARIMA model with current p and q */
				String order_new = "c(" + p + "," + d + "," + q + ")";
				re.eval("model = arima(timeseries, seasonal=list(order=" + order + ", period=" + period + "))");
		     	double AIC = re.eval("model$aic").asDouble();
		     	
		     	/* compare the new model to 
		     	 * the currently best model */
		     	if(AIC < AIC_old)
		     	{
		     		AIC_old = AIC;
		     		order = order_new;
		     	}
		     	
		     	System.out.print("*");
			}
		}
		System.out.println(" FINISHED.");
		System.out.println("Chose order = " + order + " in the end.\n");
		
		return order;
	}
	
	/**
	 * Computes a time series' autocorrelation function.
	 * @param lagMax the maximal lag to be used for the ACF.
	 * @return an array of all ACF values from 1 to lagMax.
	 */
	private double[] acf(int lagMax)
	{
		re.eval("ACF = acf(timeseries, " + lagMax + ", plot=FALSE)$acf");
		double[] ACF = new double[lagMax];
        for(int i=0; i<lagMax; i++)
        {
        	ACF[i] = re.eval("ACF[" + (i+1) + "]").asDouble();
        }	      
        return ACF;
	}
	
	/**
	 * Computes a time series' partial autocorrelation function.
	 * @param lagMax the maximal lag to be used for the PACF.
	 * @return an array of all PACF values from 1 to lagMax.
	 */
	private double[] pacf(int lagMax)
	{
		re.eval("PACF = pacf(timeseries, " + lagMax + ", plot=FALSE)$acf");
        double[] PACF = new double[lagMax];
        for(int i=0; i<lagMax; i++)
        {
        	PACF[i] = re.eval("PACF[" + (i) + "]").asDouble();
        }
        return PACF;
	}
	
	/**
	 * Determines a value for {@code p} corresponding to the specified
	 * significance level and the ACF values.
	 * @param ACF the time series' autocorrelation values.
	 * @param significanceLevel the time series' significance level.
	 * @return the determined value for p.
	 */
	private int p(double[] ACF, double significanceLevel)
	{
		int p = 0;
		for(int i=1; i< ACF.length; i++)
        {
        	if(ACF[i] > significanceLevel)
        	{
        		p = i;        		
        	}
        	else
        	{
        		return p;
        	}
        }
		return p;
	}
	
	/**
	 * Determines a value for {@code q} corresponding to the specified
	 * significance level and the PACF values.
	 * @param PACF the time series' partial autocorrelation values.
	 * @param significanceLevel the time series' significance level.
	 * @return the determined value for q.
	 */
	private int q(double[] PACF, double significanceLevel)
	{
		int q = 0;
		for(int i=1; i< PACF.length; i++)
        {
        	if(PACF[i] > significanceLevel)
        	{
        		q = i;        		
        	}
        	else
        	{
        		return q;
        	}
        }
		return q;
	}
	
	/** 
	 * Converts an array {@code [x,y,z]} to the R representation 
	 * of an array, in the form of {@code c(x,y,z)}.
	 * @param data the array to be converted.
	 * @return the R representation of the specified array.
	 */
	private String arrayToRString(Object[] data)
	{
		String s = "c(" + data[0];
		for(int i=1; i<data.length; i++)
		{
			s += "," + data[i];
		}
		s += ")";
		return s;
	}
}
