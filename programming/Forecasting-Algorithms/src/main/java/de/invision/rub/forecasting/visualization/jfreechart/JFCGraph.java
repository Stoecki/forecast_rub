package de.invision.rub.forecasting.visualization.jfreechart;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.jfree.chart.ChartFactory; 
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.visualization.Visualizer; 

/**
 * The class {@code JFCGraph} creates a window that visualizes
 * time series data. 
 */
public class JFCGraph extends ApplicationFrame implements Visualizer 
{
	private static final long serialVersionUID = 5407011388213336590L;

	private JFCMapper mapper = new JFCMapper();

	/**
	 * Constructor. 
	 */
	public JFCGraph()
	{
		super("Time Series Management");
	}

	/**
	 * Constructor specifying the graph's title.
	 * @param title the title to be set. 
	 */
	public JFCGraph(final String title)
	{
		super(title);
	}	   
	
	/**
	 * Creates a chart for the specified data.
	 * @param dataset the dataset to be visualized.
	 * @return the created chart. 
	 */
	private JFreeChart createChart(final XYDataset dataset) 
	{
		return ChartFactory.createTimeSeriesChart(             
				"Visualizing Times Series", "Time", "Value",              
				dataset, true, true, false);
	}

	/**
	 * Plots a single time series.
	 * @param timeseries the time series to be visualized. 
	 */
	@Override
	public void plotData(TimeSeriesDataset timeseries) 
	{		         	
		final JTabbedPane tabs = new JTabbedPane();
		
		for(DataPointType type : timeseries.getTypes())
		{
			final XYDataset dataset = mapper.mapDataset(timeseries, type);
			final JFreeChart chart = createChart(dataset);
			final ChartPanel chartPanel = new ChartPanel(chart);
			tabs.add(type.toString(), chartPanel);
		}
		
		final JPanel content = new JPanel(new BorderLayout());
		content.setPreferredSize(new java.awt.Dimension(600, 400));
		content.add(tabs);		
		setContentPane(content);

		this.pack();         
		RefineryUtilities.positionFrameRandomly(this);         
		this.setVisible(true);
	}

	/**
	 * Plots two time series.
	 * @param actualData the original time series.
	 * @param forecastData the predicted time series. 
	 */
	@Override
	public void plotBothData(TimeSeriesDataset actualData, TimeSeriesDataset forecastData)
	{
		final JTabbedPane tabs = new JTabbedPane();
		
		for(DataPointType type : actualData.getTypes())
		{
			final XYDataset dataset = mapper.mapDatasets(actualData, forecastData, type);
			final JFreeChart chart = createChart(dataset);
			final ChartPanel chartPanel = new ChartPanel(chart);			
			tabs.add(type.toString(), chartPanel);
			
			chart.getXYPlot().getRenderer().setSeriesPaint(0, Color.RED);
			chart.getXYPlot().getRenderer().setSeriesPaint(1, Color.LIGHT_GRAY);
		}
		
		final JPanel content = new JPanel(new BorderLayout());
		content.setPreferredSize(new java.awt.Dimension(600, 400));
		content.add(tabs);		
		setContentPane(content);

		this.pack();         
		RefineryUtilities.positionFrameRandomly(this);         
		this.setVisible(true);		
	}
}  
