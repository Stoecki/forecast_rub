package de.invision.rub.forecasting.data.iterators;

import java.util.ArrayList;
import java.util.Iterator;
import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The {@code ArrayIterator} turns a time series 
 * into a set of double arrays. 
 */
public class ArrayIterator implements Iterator<Double[]>
{
	ArrayList<Double[]> arrays;
	int index;
	
	/**
	 * Initializes the iterator with a specific time series.
	 * @param timeseries the time series to be used by the iterator.
	 */
	public void initialize(TimeSeriesDataset timeseries)
	{
		index = 0;
		Double[] array;		
		arrays = new ArrayList<Double[]>();
		
		for(TypeIterator iterator : timeseries.getTypeIterators())
		{
			array = new Double[timeseries.size()+1];
			array[timeseries.size()] = iterator.getType().getValue();
			int i = 0;
			iterator.init(timeseries);
			
			for(Double value : iterator)
			{
				array[i++] = value;
			}
			arrays.add(array);
		}
	}
	
	@Override
    public boolean hasNext() 
    {
        return index < arrays.size() && arrays.get(index) != null;
    }

    @Override
    public Double[] next() 
    {
    	return arrays.get(index++);
    }

    @Override
    public void remove() 
    {
        throw new UnsupportedOperationException();
    }	
}
