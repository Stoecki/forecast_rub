package de.invision.rub.forecasting;

import de.invision.rub.forecasting.utility.ConsoleHelper;

/**
 * The {@code FileChooser} helps the user to choose an input file.
 * A file dialog hasn't been implemented yet. Files are chosen from
 * a pre-defined list of available files, providing example data.
 */
public class FileChooser
{
	/* list of available example data sets */
	private static String[] filenames = { 
			"dt_contact_center.csv",	  // 1 year, quarter-hourly (96 values per day)
			"tel_g_offered_calls.csv",	  // 86 weeks, quarter-hourly (96 values per day)
			"ar_wil_offered_calls.csv",   // 2 years, half-hourly (48 values per day)
			"nl_spain_ac.csv"	 		  // 8 weeks, half-hourly (48 values per day)
	};
	
	/** 
	 * Getter method.
	 * @return the filepath of the input file chosen by the user. 
	 */
	public static String getFilepath()
	{	
		String filename = "de/invision/rub/forecasting/data/" + chooseFile();
		return FileChooser.class.getClassLoader().getResource(filename).getFile().substring(1);		
	}
	
	/** 
	 * Gives the user the chance to choose an input file.
	 * @return the name of the input file chosen by the user.
	 */
	private static String chooseFile()
	{
		/* prompt for input file choice */
		System.out.println("Choose an input data file:");
		for(int i=0; i<filenames.length; i++)
		{
			System.out.println(i + " : " + filenames[i]);
		}
		System.out.print("Enter a number from 0 to " + (filenames.length - 1) + ": ");
		return filenames[ConsoleHelper.readInt()];
	}
}
