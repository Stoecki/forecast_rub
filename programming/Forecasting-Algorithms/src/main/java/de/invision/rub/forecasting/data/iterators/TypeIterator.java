package de.invision.rub.forecasting.data.iterators;

import java.util.Iterator;

import de.invision.rub.forecasting.data.DataPoint;
import de.invision.rub.forecasting.data.DataPointType;
import de.invision.rub.forecasting.data.TimeSeriesDataset;

/**
 * The {@code TypeIterator} provides a time series' data for a
 * specified type.
 * @see DataPointType 
 */
public class TypeIterator implements Iterator<Double>, Iterable<Double>
{
	private TimeSeriesDataset timeseries;
	private DataPointType type;
	private int index = 0;
	
	/**
	 * Constructor specifying the iterator's type.
	 * @param type the type to be handled.
	 */
	public TypeIterator(DataPointType type)
	{
		this.type = type;
	}
	
	/**
	 * Initializes the iterator with a specific time series.
	 * @param timeseries the time series to be used by the iterator.
	 */
	public void init(TimeSeriesDataset timeseries)
	{
		this.timeseries = timeseries;
		this.index = 0;
	}
	
	/**
	 * Getter method.
	 * @return the iterator's type.
	 */
	public DataPointType getType()
	{
		return type;
	}
	
	@Override
    public boolean hasNext() 
    {
        return index < timeseries.size() && timeseries.get(index) != null;
    }

    @Override
    public Double next() 
    {
    	DataPoint datapoint = timeseries.get(index++);
        return datapoint.get(type);
    }

    @Override
    public void remove() 
    {
        throw new UnsupportedOperationException();
    }			
    
    @Override
    public Iterator<Double> iterator()
    {
    	return this;
    }
}
