package de.invision.rub.forecasting.data;

/**
 * {@code DataPointType} is an enumeration class, defining 
 * all known/allowed types of data point values. Each of those
 * types is linked to a unique double value, so that the types 
 * can be passed within a double array.
 * @see DataPoint
 */
public enum DataPointType
{
	UNDEFINED(0.0), 
	OFFERED_CALLS(1.0), 
	HANDLED_CALLS(2.0), 
	HANDLING_TIME(3.0); 
	
	private double value;

	/**
	 * Constructor.
	 * @param value the type's corresponding double value.
	 */
	private DataPointType(double value) 
	{
        this.value = value;
    }

	/**
	 * Getter method.
	 * @return the types corresponding double value.
	 */
    public double getValue() 
    {
        return value;
    }
    
    /**
     * Getter method.
     * @param value the double value for which the type shall be returned.
     * @return the type corresponding to the specified double value.
     */
    public static DataPointType get(double value)
    {
    	if(value == 1.0) 
    	{
    		return OFFERED_CALLS;
    	}
    	else if(value == 2.0) 
    	{
    		return HANDLED_CALLS;
    	}
    	else if(value == 3.0)
    	{
    		return HANDLING_TIME;
    	}
    	else
    	{
    		return UNDEFINED;
    	}
    }
}