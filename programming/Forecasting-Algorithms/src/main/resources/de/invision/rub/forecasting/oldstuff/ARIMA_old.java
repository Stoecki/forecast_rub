package de.invision.rub.forecasting.algorithms.arima.oldstuff;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import org.rosuda.JRI.Rengine;

import de.invision.rub.forecasting.algorithms.Algorithm;
import de.invision.rub.forecasting.algorithms.arima.oldstuff.maths.AutocorrelationFunctions;
import de.invision.rub.forecasting.algorithms.arima.oldstuff.maths.BoxCoxTransformation;
import de.invision.rub.forecasting.algorithms.arima.oldstuff.maths.Differencing;
import de.invision.rub.forecasting.algorithms.arima.oldstuff.maths.InformationCriteria;
import de.invision.rub.forecasting.algorithms.arima.oldstuff.maths.StationarityTesting;
import de.invision.rub.forecasting.data.TimeSeriesDataset;
import de.invision.rub.forecasting.data.DataPointType;

public class ARIMA_old implements Algorithm
{
	@Override
	public TimeSeriesDataset forecast(TimeSeriesDataset ts, int horizon) 
	{
		Map<DataPointType,Double[]> arrays = new HashMap<DataPointType,Double[]>();
		
		for(Iterator<Double[]> it = ts.getArrayIterator(); it.hasNext();)
		{
			int p = 0, d = 0, q = 0; // parameters for ArimaModel
			
			Double[] dt = it.next();
			DataPointType type = DataPointType.get(dt[ts.size()]);
			Double[] data = Arrays.copyOfRange(dt, 0, ts.size());
			
			// normalize time series by Box Cox transformation
			data = BoxCoxTransformation.apply(data);
			
			// check whether the time series data is stationary		
			// use differencing to make data stationary		
			while(!StationarityTesting.isStationary(data))
			{
				data = Differencing.apply(data);
				d++;
			}
			
			// plot ACF and PACF --> estimate possible model parameters
			double ACF = AutocorrelationFunctions.computeACF(data);
			double PACF = AutocorrelationFunctions.computePACF(data);
			/* TODO Weiterverarbeitung dieser Ergebnisse */
			p = 0; // TODO
			q = 0; // TODO
			
			// calculate AIC
			double AIC = InformationCriteria.calculateAIC(data);
			/* TODO Weiterverarbeitung dieses Ergebnisses */
			
			// plot residual --> change estimated model parameters until no lag occurs
			/* TODO ????? */
			
			// calculate forecast
			ArimaModel model = new ArimaModel(p,d,q);
			Double[] forec = doForecasting(model, data, horizon);
			arrays.put(type, forec);
		}
		
		TimeSeriesDataset fc = TimeSeriesDataset.from(arrays, ts.size(), ts.getTimeStart(), ts.getTimeInterval());		
		return fc;
}	
	
	private Double[] doForecasting(ArimaModel model, Double[] data, int horizon)
	{
		return null; // TODO
	}
	
	//#############################################################	
	
	/**
	 * White noise				ARIMA(0,0,0)
	 * Random walk				ARIMA(0,1,0) without constant
	 * Random walk with drift	ARIMA(0,1,0) with constant
	 * Autoregression			ARIMA(p,0,0)
	 * Moving average			ARIMA(0,0,q)
	 * 
	 * y'_t = c + sum(p = 1 ... P) {PHI_p * y'_t-p} + sum(q = 1 ... Q) {THETA_q * e_t-q} + e_t 
	 **/
	class ArimaModel
	{
		private int p; // order of autoregressive part
		private int d; // degree of first differencing involved
		private int q; // order of the moving average part
		
		public ArimaModel(int p, int d, int q)
		{
			this.p = p;
			this.d = d;
			this.q = q;
		}		
	}
	
	class SeasonalArimaModel extends ArimaModel
	{
		private int P, D, Q;
		
		public SeasonalArimaModel(int p, int d, int q, int P, int D, int Q)
		{
			super(p,d,q);
			this.P = P;
			this.D = D;
			this.Q = Q;
		}
	}
}
