package de.invision.rub.forecasting.algorithms.arima.oldstuff.maths;

public class StationarityTesting 
{	
	public static boolean isStationary(Double[] originalData)
	{
		// Augmented Dickey-Fuller Test
		Double[] differencedData = Differencing.apply(originalData);
		
		int T1 = originalData.length;
		int T2 = differencedData.length;
		
		int K = (int) Math.floor(Math.pow((T1-1.0),(1.0/3.0)));
		double[] BETA = new double[K]; // TODO ???
		
		// y'_t = PHI * y_t-1 + sum(k = 1 ... K) { BETA_k * y'_t-k }
		// differencedData[t] = PHI * originalData[t-1] + sum(k = 1 ... K) { BETA_k * differencedData[t-k] }		
		// PHI = ( differencedData[t] - sum(k = 1 ... K) { BETA_k * differencedData[t-k] } ) / originalData[t-1] 
		
		for(int t=K; t<T2; t++)
		{			
			double sum = 0.0;
			for(int k=1; k<=K; k++)
			{
				sum += BETA[k] * differencedData[t-k];
			}			
			double PHI = ( differencedData[t] - sum ) / originalData[t-1];
			
			if (PHI > 0.05)
			{
				return false;
			}
		}
		
		return true;
	}	
}
