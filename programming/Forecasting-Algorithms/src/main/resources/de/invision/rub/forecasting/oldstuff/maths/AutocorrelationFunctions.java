package de.invision.rub.forecasting.algorithms.arima.oldstuff.maths;

/* ACF und PACF 
 * Autocorrelation und Partial Autocorrelation */
public class AutocorrelationFunctions
{
	/**
	 * p(0) = 1
	 * p(T) = 0   << unkorreliert
	 * p(T) = +1  << gleichlaufend linear abh�ngig
	 * p(T) = -1  << gegenl�ufig linear abh�ngig
	 **/
	public static double computeACF(Double[] originalData)
	{
		int n = originalData.length;
		double my = 0.0, var = 0.0, cov = 0.0, p = 0.0;
		int T = 0; // lag TODO
		
		// � = 1/n * sum(t = 1 ... n) { Y_t }
		for(int i=0; i<n; i++)
		{
			my += originalData[i];
		}
		my /= n;
		
		// Var(Y_t) = 1/n * sum(t = 1 ... n) { (Y_t - �) ^ 2 }
		for(int i=0; i<n; i++)
		{
			var += Math.pow(originalData[i], 2.0);
		}
		var /= n;
		
		// Cov(Y_t, Y_t+T) = 1/n * sum(t = 1 ... n-T) { (Y_t - �) * (Y_t+T - �) }
		for(int i=0; i<(n-T); i++)
		{
			cov += (originalData[i] - my) * (originalData[i+T] - my);
		}
		cov /= n;
		
		// p(T) = y(T) / y(0) = Cov(Y_t, Y_t+T) / Var(Y_t)
		p = cov / var;
		
		return p;
	}
	
	public static double computePACF(Double[] originalData)
	{
		// TODO
		return 0.0;
	}
	
	/*
	public void autoCorrelation(int size, double[] x)
	{
	    float[] R = new float[size];
	    float sum;

	    for(int i=0; i<size; i++) 
	    {
	        sum = 0;
	        for(int j=0; j<size-i; j++) 
	        {
	            sum += x[j] * x[j+i];
	        }
	        R[i] = sum;
	    }
	}
	*/
	
	/*
	public void acf()
	{
		try
		{
			int n = 110;
			double x_avg = 6.38; // average of the data
			List<String> x_t = new ArrayList<String>();
			

			// String[] xt = (String[]) x_t.toArray();
			String[] xt = x_t.toArray(new String[x_t.size()]);
			// for (String x : xt){
			String[] xth = new String[xt.length];
			// for (int i=0; i<=xt.length; i++)
			// xth[i] = xt[xt.length - i - 1];

			double[] m = new double[xt.length]; // sum of y(h)
			double[] p = new double[xt.length]; // temporary data
			double[] l = new double[xt.length]; // temporary data
			double[] y = new double[xt.length]; // ACF
			double variance = 1.25;

			for (int h = 0; h < xt.length; h++)
			{
				for (int t = 1; t < xt.length - h; t++)
				{
					p[h] = p[h] + ((Double.parseDouble(xt[t + h])) - x_avg) * ((Double.parseDouble(xt[t - 1])) - x_avg);
					m[h] = (1.0 / xt.length) * p[h];
				}
				y[h] = m[h] / variance;
			}
		} 
		catch (Exception ex) {}
	}
	*/
}
