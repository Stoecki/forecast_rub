package de.invision.rub.forecasting.algorithms.arima.oldstuff.maths;

public class Differencing 
{
	public static Double[] apply(Double[] originalData)
	{
		Double[] differencedData = new Double[originalData.length-1];
		
		for(int i=0; i<differencedData.length; i++)
		{
			differencedData[i] = originalData[i+1] - originalData[i];
		}
		
		return differencedData;
	}
	
	/*
	  public Double[] diffInv(Double[] differencedData)
	{
		Double[] reversedData = new Double[differencedData.length+1];
		
		for(int i=0; i<reversedData.length; i++)
		{
			reversedData[i] = differencedData[i+1] - differencedData[i];
		}
		
		return reversedData;
	}
	*/
}
