package de.invision.rub.forecasting.algorithms.arima.oldstuff.maths;

public class BoxCoxTransformation
{
	public static Double[] apply(Double[] originalData)
	{
		double lambda = 0.0; //TODO w�hle gutes Lambda
		Double[] transformedData = transform(lambda, originalData);
		return transformedData;
	}

	public static Double[] transform(double lambda, Double[] originalData)
	{
		Double[] transformedData = new Double[originalData.length];

		for(int i=0; i<originalData.length; i++)
		{
			if(lambda == 0)
			{
				//TODO Handle the case when originalData[i] == 0
				transformedData[i] = Math.log(originalData[i]+1);
			} else
			{
				transformedData[i] = (Math.pow(originalData[i], lambda) - 1.0) / lambda;
			}
		}

		return transformedData;
	}
}

