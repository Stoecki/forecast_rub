package de.invision.rub.forecasting.algorithms.arima.oldstuff.maths;

public class InformationCriteria 
{
	/* Akaike Information Criterion */
	public static double calculateAIC(Double[] data)
	{
		// To determine the order of a non-seasonal ARIMA model, a useful 	
		// criteria is Akaike information criterion (AIC) . It is written as:
	
		// AIC = -2 * log(L) + 2 * (p+q+k+1), where L is the likelihood of the data, 
		// p is the order of the autoregressive part and q is the order of the moving 
		// average part. The k term in these criterion are defined as the number of 
		// parameters in the model being fitted to the data. For AIC, if k = 1 then 
		// c != 0 and if k = 0 then c = 0.
	
		// The corrected AIC for ARIMA models can be written as:
		// AICc = AIC + (2 * (p+q+k+1) * (p+q+k+2)) / (T-p-q-k-2)

		return 0.0;
	}
}
