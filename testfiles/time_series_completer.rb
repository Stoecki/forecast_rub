# Usage:
# ruby time_series_completer.rb file_name > new_file_name

require 'time'

if ARGV.empty?
  abort "Missing filename"
end

class DataFiller
  def initialize(value_count, interval)
    @interval = interval
    @value_count = value_count
  end

  def fill_time_stamps(last_existing_time, next_existing_time)
    zero_values = Array.new(@value_count) { 0 }.join(',')
    current_time = last_existing_time + 900
    while current_time < next_existing_time
      puts "#{current_time.to_s},#{zero_values}"
      current_time = current_time + 900
    end
  end
end

file_name = ARGV[0]

file = File.new(file_name, "r")

# write headers back
puts file.gets
puts file.gets

previous_line = file.gets
puts previous_line

interval = nil
data_filler = nil

while (current_line = file.gets)
  previous_line_params = "#{previous_line}".split(',')
  current_line_params = "#{current_line}".split(',')

  current_timestamp = Time.parse(current_line_params[0])
  previous_timestamp = Time.parse(previous_line_params[0])

  # we assume the first two timestamps define the interval
  unless interval
    interval = current_timestamp - previous_timestamp
  end

  # And initialize the DataFiller with that interval
  unless data_filler
    data_filler = DataFiller.new(
      current_line_params.size - 1, interval)
  end

  if current_timestamp - previous_timestamp != interval
    data_filler.fill_time_stamps(previous_timestamp, current_timestamp)
  end

  puts current_line

  previous_line = current_line
end

file.close
